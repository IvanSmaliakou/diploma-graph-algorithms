#include <iostream>
#include <vector>
#include <map>

using namespace std;

void BFS(map<int, vector<int>> graph, map<int, bool>& visitedMap, vector<int>& resultPath, int currentNode) {
    visitedMap[currentNode] = true;
    resultPath.push_back(currentNode);
    for (vector<int>::iterator it = graph[currentNode].begin(); it != graph[currentNode].end(); it++)
    {
        if (!visitedMap[*it]) {
            BFS(graph, visitedMap, resultPath, *it);
            resultPath.push_back(currentNode);
        }
    }
}

void fillGraph(map<int, vector<int>>& graph)
{
    for (map<int, vector<int>>::iterator it = graph.begin(); it != graph.end(); it++) {
        for (vector<int>::iterator vector_it = it->second.begin(); vector_it != it->second.end(); vector_it++) {
            graph[*vector_it].push_back(it->first);
        }
    }
}

int main()
{
    map<int, vector<int>> graph{ {1, {2}}, {2, {3,4}}, {3, {8,6,7}}, {4, {5}}, {6, {7}}, {8, {1}} };
    fillGraph(graph);
    map<int, bool> visited;
    vector<int> resultPath;
    BFS(graph, visited, resultPath, 1);
    cout << "dfs full path is: ";
    for (vector<int>::iterator it = resultPath.begin(); it!=resultPath.end(); it++)
    {
        cout << *it;
        // if not last element in vector
        if (&*it != &resultPath.back())
            cout << " -> ";
    }
    cout << std::endl;
}
