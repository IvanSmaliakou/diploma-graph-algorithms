#include <iostream>
#include <map>
#include <vector>

using namespace std;

void fillGraph(map<int, vector<int>>& graph);
void printPath(vector<int> path);
vector<int> BFS(map<int, vector<int>> graph, int starting_node_number);

int main()
{
    map<int, vector<int>> graph{ {1, {2}}, {2, {3,4}}, {3, {8,6,7}}, {4, {5}}, {6, {7}}, {8, {1}} };
    fillGraph(graph);
    vector<int> result = BFS(graph, 1);
    printPath(result);
}

void fillGraph(map<int, vector<int>>& graph)
{
    for (map<int, vector<int>>::iterator it = graph.begin(); it != graph.end(); it++) {
        for (vector<int>::iterator vector_it = it->second.begin(); vector_it != it->second.end(); vector_it++) {
            graph[*vector_it].push_back(it->first);
        }
    }
}

void printPath(vector<int> path) {
    for (vector<int>::iterator it = path.begin(); it != path.end() - 1; ++it)
    {
        cout << *it << " -> ";
    }
    cout << *(path.end() - 1) << endl;
}


vector<int> BFS(map<int, vector<int>> graph, int starting_node_number) {
    vector<int> resultPath;
    vector<int> processing_queue;
    map<int, bool> visited_nodes;

    processing_queue.push_back(starting_node_number);
    visited_nodes[starting_node_number] = true;

    while (!processing_queue.empty())
    {
        int current_node = processing_queue.front();
        processing_queue.erase(processing_queue.begin());

        resultPath.push_back(current_node);

        for (size_t i = 0; i < graph[current_node].size(); i++)
        {
            int neighbor = graph[current_node][i];
            if (!visited_nodes[neighbor]) {
                visited_nodes[neighbor] = true;
                processing_queue.push_back(neighbor);
            }
        }
    }
    return resultPath;
}