using GraphAlgorithms.Algorithms;
using GraphAlgorithms.Drawing;
using System.Reflection;
using System.Text;

namespace GraphAlgorithms
{
    // AlgoType is an enum, that represents any provided algorithm type.
    public enum AlgoType
    {
        BFS,
        DFS
    }
    public enum PanelTypes
    {
        ProblemArea,
        CodeArea,
        DrawingArea
    }

    public delegate Graphics CreateGraphicsDelegate();
    public partial class BootstrapForm : Form
    {
        private bool clickedOnce { get; set; }

        // Graph is a programmic graph representation;
        private Graph _graph;

        // _defaultButtonsAreaPanelWidth is a default static buttons area panel width. It's not changing depending on size of the App.
        private readonly int _defaultButtonsAreaPanelWidth = 292;

        // _defaultLowerBoundOffset is the number of pixels on which the form height is more then all evements height.
        private readonly int _defaultLowerBoundFormOffset = 48;

        // GraphicsElementChangeState is an event that will be triggered when any graph element is clicked; it signilizes, that element's state has been changed.
        public event ChangeSelectedElementState GraphicsElementChangeState;

        // DenyDownedButton will deny any mouse downed GraphicsElement.
        public event DenyDownedButton DenyDownedButton;

        // IncreaseWeight is an increase weight event.
        public event ChangeWeight IncreaseWeight;

        // IncreaseWeight is an decrease weight event.
        public event ChangeWeight DecreaseWeight;

        // CreateGraphics is a delegate for creating new graphics during each drawing operation.
        public CreateGraphicsDelegate CreateGraphics;

        // AlgorithmType is a selected algorithm type.
        public AlgoType? AlgorithmType;

        // CurrentPanelTypes is a current types of panels, which are displayed on the working area.
        private List<PanelTypes> CurrentPanelTypes = new List<PanelTypes> { PanelTypes.DrawingArea };

        // CancelationTokenSource is a cancelation token constructor.
        public CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

        // AlgoResult is an algorithm result, which has to be propagated through methods.
        public List<Node> AlgoResult;

        // CurrentAlgoResultNodeIndex is a current index of the blinking node.
        public int CurrentAlgoResultBlinkingNodeIndex { get; set; } = 0;

        private int _defaultAlgoPathTextBoxHeight = 132;

        public BootstrapForm()
        {
            this.CreateGraphics = this.drawingAreaPanel.CreateGraphics;
            this._graph = new Graph(this.CreateGraphics);
            InitializeComponent();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Back:
                case Keys.Delete:
                    this._graph.RemoveSelectedGraphicsElement(ref this.GraphicsElementChangeState, ref this.DenyDownedButton, ref this.IncreaseWeight,
                                            ref this.DecreaseWeight);
                    break;
                case Keys.Up:
                    this.IncreaseWeight?.Invoke();
                    break;
                case Keys.Down:
                    this.DecreaseWeight?.Invoke();
                    break;
            }
            this._graph.Redraw();
            if (keyData == Keys.Up || keyData == Keys.Down)
                return true;
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void OnResize(object sender, EventArgs e)
        {
            // panels sizes and locations.
            int onePanelWidth = (this.Size.Width - this.buttonsAreaPanel.Size.Width) / this.CurrentPanelTypes.Count;
            if (this.CurrentPanelTypes.Contains(PanelTypes.ProblemArea))
            {
                this.problemAreaPanel.Size = new Size(onePanelWidth, this.Height - this._defaultLowerBoundFormOffset);
            }
            else {
                this.problemAreaPanel.Size = new Size(0, this.Height - this._defaultLowerBoundFormOffset);
            }
            if (this.CurrentPanelTypes.Contains(PanelTypes.CodeArea))
            {
                this.codeAreaPanel.Size = new Size(onePanelWidth, this.Height - this._defaultLowerBoundFormOffset);
            }
            else
            {
                this.codeAreaPanel.Size = new Size(0, this.Height - this._defaultLowerBoundFormOffset);
            }
            // drawing area is always active.
            this.drawingAreaPanel.Size = new Size(onePanelWidth, this.Height - this._defaultLowerBoundFormOffset);

            this.problemAreaPanel.Location = new Point(this.buttonsAreaPanel.Width, 0);
            this.codeAreaPanel.Location = new Point(this.buttonsAreaPanel.Width + this.problemAreaPanel.Width, 0);
            this.drawingAreaPanel.Location = new Point(this.buttonsAreaPanel.Width + this.problemAreaPanel.Width + this.codeAreaPanel.Width, 0);

            this.buttonsAreaPanel.Height = this.Size.Height - this._defaultLowerBoundFormOffset;

            // button sizes and locations.
            this.buttonCollapseExpandMenu.Height = this.Size.Height - this._defaultLowerBoundFormOffset;
            this.buttonAbout.Location = new Point(this.buttonAbout.Location.X, this.Size.Height - this._defaultLowerBoundFormOffset - this.buttonAbout.Height);

            // ALGORITHM CONTROL BUTTONS ARE BINDED TO THE DRAWING_AREA_PANEL.
            this.buttonPreviousNode.Location = new Point(this.drawingAreaPanel.Size.Width / 2 - this.buttonPreviousNode.Width, this.buttonPreviousNode.Location.Y);
            this.buttonNextNode.Location = new Point(this.drawingAreaPanel.Size.Width / 2, this.buttonNextNode.Location.Y);

            // textbox sizes and locations.
            this.algoPathTextBox.Location = new Point(this.drawingAreaPanel.Size.Width / 10, this.Height - this._defaultLowerBoundFormOffset - this.algoPathTextBox.Height);
            this.algoPathTextBox.Width = (int)(this.drawingAreaPanel.Size.Width * 0.8);

            // for compatability on other machines.
            if (this._graph == null) {
                return;
            }
            // nodes sizes and locations.
            foreach (Node n in this._graph.Nodes) {
                n.NewCenter((int)(n.XToWidth * this.drawingAreaPanel.Width), (int)(n.YToHeight * this.drawingAreaPanel.Height), n.XToWidth, n.YToHeight);
            }
            this._graph.Redraw();
        }

        private void OnButtonCollapseExpandMenuClick(object sender, EventArgs e)
        {
            // if collapsed, then expand.
            if (this.buttonCollapseExpandMenu.Dock.Equals(DockStyle.Left))
            {
                this.buttonsAreaPanel.Size = new Size(this._defaultButtonsAreaPanelWidth, this.Size.Height);
                this.buttonCollapseExpandMenu.Dock = DockStyle.None;
                this.buttonCollapseExpandMenu.Image = Properties.Resources.icons8_�������_�������_�����_26;
            }
            else
            {
                this.buttonCollapseExpandMenu.Dock = DockStyle.Left;
                this.buttonCollapseExpandMenu.Image = Properties.Resources.icons8_�������_�������_�����_261;
                this.buttonsAreaPanel.Width = this.buttonCollapseExpandMenu.Size.Width;
            }
            // better to call OnResize, since panels width really resized.
            OnResize(null, null);
        }

        private void OnDrawingPanelMouseDown(object sender, MouseEventArgs e)
        {
            Object obj = _graph.GetObjectByCoords(e.X, e.Y);
            // handler for left mouse button down
            if (e.Button.Equals(MouseButtons.Left))
            {
                switch (obj)
                {
                    case Node n:
                        n.LeftButtonMouseDowned = true;
                        break;
                    case Edge edge:
                        edge.LeftButtonMouseDowned = true;
                        break;
                }
            }
            // handler for right mouse button down
            if (e.Button.Equals(MouseButtons.Right) && obj is Edge rightBtnDownedEdge)
            {
                rightBtnDownedEdge.RightButtonMouseDowned = true;
            }
        }

        private void OnDrawingPanelMouseUp(object sender, MouseEventArgs e)
        {
            // clickedOnSpaceOnThisIteration is a flag that signalizes, that there was a click on space on this MouseUp event.
            bool clickedOnSpaceOnThisIteration = false;
            Object obj = _graph.GetObjectByCoords(e.X, e.Y);
            switch (obj)
            {
                case Node n:
                    if (e.Button.Equals(MouseButtons.Left))
                    {
                        if (n.LeftButtonMouseDowned)
                        {
                            this.GraphicsElementChangeState?.Invoke(n);
                        }
                        else
                        {
                            Object? mouseDownedObject = _graph.GetLeftMouseDownedObject();
                            if (mouseDownedObject != null)
                            {
                                switch (mouseDownedObject)
                                {
                                    case Node mouseDownedNode:
                                        Edge newEdge = new Edge(ref this.GraphicsElementChangeState, ref this.DenyDownedButton, ref this.IncreaseWeight,
                                            ref this.DecreaseWeight, mouseDownedNode, n);
                                        this._graph.AddEdge(newEdge);
                                        break;
                                }
                            }
                        }
                    }
                    break;
                case Edge edge:
                    if (edge.LeftButtonMouseDowned && e.Button.Equals(MouseButtons.Left))
                    {
                        this.GraphicsElementChangeState?.Invoke(edge);
                    }
                    if (edge.RightButtonMouseDowned && e.Button.Equals(MouseButtons.Right))
                    {
                        edge.ChangeArrowState();
                    }
                    break;

                // click on space
                default:
                    if (e.Button.Equals(MouseButtons.Left))
                    {
                        if (this.clickedOnce)
                        {
                            double xToWidth = e.X / (double)this.drawingAreaPanel.Width;
                            double yToHeight = e.Y / (double)this.drawingAreaPanel.Height;
                            Node newNode = new Node(ref this.GraphicsElementChangeState, ref this.DenyDownedButton, e.X, e.Y, xToWidth, yToHeight);
                            this._graph.AddNode(newNode);
                        }
                        else
                        {
                            this._graph.DenySelectedByAlgorithmNodes();
                            this.clickedOnce = true;
                            clickedOnSpaceOnThisIteration = true;
                        }
                    }
                    this.GraphicsElementChangeState?.Invoke(null);
                    break;
            }
            if (!clickedOnSpaceOnThisIteration)
            {
                this.clickedOnce = false;
            }

            // deny all downed buttons and redraw the graph.
            this.DenyDownedButton?.Invoke();
            this._graph.Redraw();
        }

        private void OnDrawingPanelMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                Node? clickedNode = this._graph.GetSelectedNode();
                if (clickedNode != null && clickedNode.Selected)
                {
                    double xToWidth = e.X / (double)this.drawingAreaPanel.Width;
                    double yToHeight = e.Y / (double)this.drawingAreaPanel.Height;
                    clickedNode.NewCenter(e.X, e.Y, xToWidth, yToHeight);

                    // deny all downed buttons and redraw the graph.
                    this.DenyDownedButton?.Invoke();
                    this._graph.Redraw();
                }
            }
        }

        private void buttonPreviousNodeClick(object sender, EventArgs e)
        {
            this.CancellationTokenSource.Cancel();

            IEnumerable<Node> previousAlgoResultNodes = this.AlgoResult.Take(this.CurrentAlgoResultBlinkingNodeIndex);
            if (!previousAlgoResultNodes.Contains(this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex])) {
                this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].SelectedByAlgorithm = false;
            }
            this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].CurrentlyAlgoSelected = false;
            this.CurrentAlgoResultBlinkingNodeIndex--;
            this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].CurrentlyAlgoSelected = true;
            this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].SelectedByAlgorithm = true;

            if (this.CurrentAlgoResultBlinkingNodeIndex == 0)
            {
                this.buttonPreviousNode.Enabled = false;
            }
            this.buttonNextNode.Enabled = true;

            this.CancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = this.CancellationTokenSource.Token;
            Task task = new Task(() =>
            {
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                    this._graph.Redraw();
                    Thread.Sleep(500);
                }
            }, token);
            task.Start();
        }

        private void buttonNextNodeClick(object sender, EventArgs e)
        {
            this.CancellationTokenSource.Cancel();

            this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].CurrentlyAlgoSelected = false;
            this.CurrentAlgoResultBlinkingNodeIndex++;
            this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].CurrentlyAlgoSelected = true;
            this.AlgoResult[this.CurrentAlgoResultBlinkingNodeIndex].SelectedByAlgorithm = true;

            if (this.CurrentAlgoResultBlinkingNodeIndex == this.AlgoResult.Count - 1)
            {
                this.buttonNextNode.Enabled = false;
            }
            if (this.CurrentAlgoResultBlinkingNodeIndex == 1)
            {
                this.buttonPreviousNode.Enabled = true;
            }

            this.CancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = this.CancellationTokenSource.Token;
            Task task = new Task(() =>
            {
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                    this._graph.Redraw();
                    Thread.Sleep(500);
                }
            }, token);
            task.Start();
        }

        private void buttonStartClick(object sender, EventArgs e)
        {
            Node? selectedNode = this._graph.GetSelectedNode();
            if (this.AlgorithmType == null || selectedNode == null) {
                return;
            }

            // actualize buttons state.
            this.buttonStop.Enabled = true;
            this.buttonNextNode.Enabled = true;
            this.buttonStart.Enabled = false;


            // add code panel.
            if (!this.CurrentPanelTypes.Contains(PanelTypes.CodeArea))
            {
                this.CurrentPanelTypes.Add(PanelTypes.CodeArea);
            }
            List<Node>? results = null;
            switch (this.AlgorithmType)
            {
                case AlgoType.DFS:
                    DFS dfsService = new DFS(this._graph);
                    selectedNode = this._graph.GetSelectedNode();
                    if (selectedNode == null)
                    {
                        return;
                    }
                    results = dfsService.MakeUndirectedDFS(selectedNode.NodeNumber);
                    break;
                case AlgoType.BFS:
                    BFS bfsService = new BFS(this._graph);
                    selectedNode = this._graph.GetSelectedNode();
                    if (selectedNode == null)
                    {
                        return;
                    }
                    results = bfsService.MakeUndirectedBFS(selectedNode.NodeNumber);
                    break;
            }

            // unselect all nodes.
            this.GraphicsElementChangeState?.Invoke(null);

            if (results == null || results.Count() == 0)
            {
                return;
            }
            if (results.Count() == 1) {
                this.buttonNextNode.Enabled = false;
            }
            this.AlgoResult = results;

            Node currentNode = results[0];
            currentNode.SelectedByAlgorithm = true;
            currentNode.CurrentlyAlgoSelected = true;

            this.CancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = this.CancellationTokenSource.Token;
            Task task = new Task(() =>
            {
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                    this._graph.Redraw();
                    Thread.Sleep(500);
                }
            }, token);
            task.Start();

            StringBuilder sb = new StringBuilder();
            sb.Append("���� ������ �����: ");
            foreach (Node node in results)
            {
                sb.AppendFormat("{0} -> ", node.NodeNumber);
            }
            sb.Remove(sb.Length - 4, 4);

            this.algoPathTextBox.Enabled = true;
            this.algoPathTextBox.Height = _defaultAlgoPathTextBoxHeight;
            this.algoPathTextBox.Text = sb.ToString();

            this.OnResize(null, null);
        }

        private void buttonStopClick(object sender, EventArgs e)
        {
            this.buttonStart.Enabled = true;
            this.buttonStop.Enabled = false;
            this.buttonPreviousNode.Enabled = false;
            this.buttonNextNode.Enabled = false;
            this.algoPathTextBox.Enabled = false;
            this.algoPathTextBox.Text = "";
            this.algoPathTextBox.Height = 0;

            this.CurrentPanelTypes.Remove(PanelTypes.CodeArea);
            this.CurrentAlgoResultBlinkingNodeIndex = 0;
            this._graph.DenySelectedByAlgorithmNodes();
            this.AlgoResult = null;
            this.CancellationTokenSource.Cancel();
            this.OnResize(null, null);
        }

        private void buttonDFSClick(object sender, EventArgs e)
        {
            // if already selected.
            if (this.AlgorithmType == AlgoType.DFS) {
                this.buttonDFS.BackColor = SystemColors.Control;
                this.CurrentPanelTypes.Remove(PanelTypes.ProblemArea);
                this.AlgorithmType = null;
                this.OnResize(null, null);
                return;
            }

            this.buttonDFS.BackColor = Color.Red;
            this.bfsButton.BackColor = SystemColors.Control;
            this.buttonAbout.BackColor = SystemColors.Control;

            this.AlgorithmType = AlgoType.DFS;
            if (!this.CurrentPanelTypes.Contains(PanelTypes.ProblemArea))
            {
                this.CurrentPanelTypes.Add(PanelTypes.ProblemArea);
            }
            this.OnResize(null, null);

            string currentDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var sr = new StreamReader(String.Format(@"{0}\Resources\dfs_description.txt", currentDir));
            List<string> description = new List<string>();
            while (!sr.EndOfStream)
            {
                string? line = sr.ReadLine();
                description.Add(line);
            }
            sr.Close();
            this.problemAreaTextBox.Lines = description.ToArray();
            this.codeAreaTextBox.Text = File.ReadAllText(String.Format(@"{0}\Resources\dfs.cpp", currentDir));
        }

        private void bfsButtonClick(object sender, EventArgs e)
        {
            // if already selected.
            if (this.AlgorithmType == AlgoType.BFS)
            {
                this.bfsButton.BackColor = SystemColors.Control;
                this.CurrentPanelTypes.Remove(PanelTypes.ProblemArea);
                this.AlgorithmType = null;
                this.OnResize(null, null);
                return;
            }
            this.bfsButton.BackColor = Color.Red;
            this.buttonDFS.BackColor = SystemColors.Control;
            this.buttonAbout.BackColor = SystemColors.Control;
            this.AlgorithmType = AlgoType.BFS;

            if (!this.CurrentPanelTypes.Contains(PanelTypes.ProblemArea))
            {
                this.CurrentPanelTypes.Add(PanelTypes.ProblemArea);
            }
            this.OnResize(null, null);
            
            string currentDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var sr = new StreamReader(string.Format(@"{0}\Resources\bfs_description.txt", currentDir));
            List<string> description = new List<string>();
            while (!sr.EndOfStream)
            {
                string? line = sr.ReadLine();
                description.Add(line);
            }
            sr.Close();
            this.problemAreaTextBox.Lines = description.ToArray();
            this.codeAreaTextBox.Text = File.ReadAllText(string.Format(@"{0}\Resources\bfs.cpp", currentDir));
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            // if already selected.
            if (this.buttonAbout.BackColor == Color.Red)
            {
                this.buttonAbout.BackColor = SystemColors.Control;
                this.CurrentPanelTypes.Remove(PanelTypes.ProblemArea);
                this.OnResize(null, null);
                return;
            }

            this.buttonAbout.BackColor = Color.Red;
            this.bfsButton.BackColor = SystemColors.Control;
            this.buttonDFS.BackColor = SystemColors.Control;

            string currentDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var sr = new StreamReader(string.Format(@"{0}\Resources\program_manual.txt", currentDir));
            List<string> programManual = new List<string>();
            while (!sr.EndOfStream)
            {
                string? line = sr.ReadLine();
                programManual.Add(line);
            }
            sr.Close();
            if (!this.CurrentPanelTypes.Contains(PanelTypes.ProblemArea))
            {
                this.CurrentPanelTypes.Add(PanelTypes.ProblemArea);
            }
            this.problemAreaTextBox.Lines = programManual.ToArray();
            this.OnResize(null, null);
        }
    }
}