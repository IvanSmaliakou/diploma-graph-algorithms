﻿namespace GraphAlgorithms.Exceptions
{
    public class NoSuchObjectException : Exception
    {
        private static readonly string _classMessage = "There is no object with such coordinates: {0}, {1}.";
        public NoSuchObjectException()
        {

        }

        public NoSuchObjectException(int x, int y) : base(String.Format(_classMessage, x, y))
        {

        }
    }
}
