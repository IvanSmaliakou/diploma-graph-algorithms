﻿using System.Drawing.Drawing2D;

namespace GraphAlgorithms.Drawing
{
    // ChangeWeight is a method template of changing edge's weight.
    public delegate void ChangeWeight();

    public class Edge : IGraphicsElement
    {
        public enum Direction : int
        {
            FORWARD,
            BACKWARD,
            NONE
        }

        // Weight is the edge's weight.
        public int Weight { get; set; }

        // direction is an arrow direction.
        public Direction ArrowDirection { get; set; } = Edge.Direction.NONE;

        // normalFillColor represents color of normal state edge.
        private readonly Color _normalFillColor = Color.Gray;
        public long LatestActionTS { get; set; }

        // FromNode is a starting node of the edge.
        public Node FromNode { get; set; }

        // ToNode is the finishing node of the edge.
        public Node ToNode { get; set; }

        // DrawPointFrom is a point in which the edge starts on the graphical area.
        public Point DrawPointFrom { get; set; }

        // DrawPointTo is a point in which the edge ends on the graphical area.
        private Point DrawPointTo { get; set; }

        // _width is a line width; consider using odd numbers to simplyfy borders dectection
        private readonly Single _width = 9;

        // _fontSize is a text font size. Used in Draw method.
        private readonly int _fontSize = 30;

        // selectedFillColor represents color of selected edge.
        private readonly Color _selectedFillColor = Color.Red;

        private readonly Color _weightColor = Color.Blue;

        private bool _selected = false;

        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (value)
                {
                    this.LatestActionTS = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                }
                this._selected = value;
            }
        }

        // MouseDowned indicates that mouse has been clicked, but not released.
        public bool LeftButtonMouseDowned { get; set; }
        public bool RightButtonMouseDowned { get; set; }

        // DenyAllDownedButtons will deny both left and right mouse buttons.
        public void DenyAllDownedButtons()
        {
            this.LeftButtonMouseDowned = false;
            this.RightButtonMouseDowned = false;
        }
        public void ChangeEdgesState(IGraphicsElement clickedEdge)
        {
            if (this.Equals(clickedEdge))
            {
                this.Selected = !this.Selected;
                return;
            }
            this.Selected = false;
        }
        // IncreaseWeight increases Weight property by one.
        public void IncreaseWeight()
        {
            if (this.Selected)
                this.Weight++;
        }

        // DecreaseWeight decreases Weight property by one.
        public void DecreaseWeight()
        {
            if (this.Selected)
                this.Weight--;
        }

        // SetNewDrawPointsCoordinates sets new draw points coordinates according to new nodes position.
        public void SetNewDrawPointsCoordinates()
        {
            Tuple<Point, Point> drawCoordinates = this.GetDrawPointsCoordinates();
            this.DrawPointFrom = drawCoordinates.Item1;
            this.DrawPointTo = drawCoordinates.Item2;
        }

        // ReturnEdgesConnectedWithNode returns all edges connected with current node
        public void ReturnEdgesConnectedWithNode(Node node, NodeDeletedEventArgs eventArgs)
        {
            if (this.FromNode.Equals(node) || this.ToNode.Equals(node))
            {
                eventArgs.Edges.Add(this);
            }
        }


        // GetNeighborNodes adds neighbor node to eventArgs subsequent list, if the provided nodeNumber is equal to the number of either FromNode or ToNode.
        public void GetNeighborNodes(int nodeNumber, NodeNeighborsEventArgs eventArgs)
        {
            if (this.FromNode.NodeNumber.Equals(nodeNumber))
                eventArgs.nodes.Add(this.ToNode);
            else if (this.ToNode.NodeNumber.Equals(nodeNumber))
                eventArgs.nodes.Add(this.FromNode);

        }

        public Edge(ref ChangeSelectedElementState changeNodesStateEvent, ref DenyDownedButton denyButton, ref ChangeWeight increaseWeight,
            ref ChangeWeight decreaseWeight, Node fromNode, Node toNode)
        {
            // event handlers initialization
            increaseWeight += this.IncreaseWeight;
            decreaseWeight += this.DecreaseWeight;
            denyButton += this.DenyAllDownedButtons;
            changeNodesStateEvent += this.ChangeEdgesState;

            fromNode.NodeCenterChanged += this.SetNewDrawPointsCoordinates;
            toNode.NodeCenterChanged += this.SetNewDrawPointsCoordinates;


            this.FromNode = fromNode;
            this.ToNode = toNode;

            this.SetNewDrawPointsCoordinates();
        }

        private Tuple<Point, Point> GetDrawPointsCoordinates()
        {
            int dX = Math.Abs(this.FromNode.Center.X - this.ToNode.Center.X);
            int dY = Math.Abs(this.FromNode.Center.Y - this.ToNode.Center.Y);
            int distance = (int)Math.Sqrt(dX * dX + dY * dY);
            double decreaseCoeff = (double)(distance - this.FromNode.Diameter / 2) / (double)distance;
            double newAX = 0;
            double newBX = 0;
            double newAY = 0;
            double newBY = 0;
            if (this.FromNode.Center.X >= this.ToNode.Center.X)
            {
                newAX = this.ToNode.Center.X + decreaseCoeff * dX;
                newBX = this.FromNode.Center.X - decreaseCoeff * dX;
            }
            else
            {
                newBX = this.FromNode.Center.X + decreaseCoeff * dX;
                newAX = this.ToNode.Center.X - decreaseCoeff * dX;
            }
            if (this.FromNode.Center.Y >= this.ToNode.Center.Y)
            {
                newAY = this.ToNode.Center.Y + decreaseCoeff * dY;
                newBY = this.FromNode.Center.Y - decreaseCoeff * dY;
            }
            else
            {
                newBY = this.FromNode.Center.Y + decreaseCoeff * dY;
                newAY = this.ToNode.Center.Y - decreaseCoeff * dY;
            }
            Point ret1 = new Point((int)newAX, (int)newAY);
            Point ret2 = new Point((int)newBX, (int)newBY);
            return new Tuple<Point, Point>(ret1, ret2);
        }

        public void Draw(Graphics g)
        {
            using (var pen = new Pen(this._selected ? _selectedFillColor : _normalFillColor, this._width))
            {
                switch (this.ArrowDirection)
                {
                    case Direction.FORWARD:
                        pen.StartCap = LineCap.Flat;
                        pen.EndCap = LineCap.ArrowAnchor;
                        break;
                    case Direction.BACKWARD:
                        pen.StartCap = LineCap.ArrowAnchor;
                        pen.EndCap = LineCap.Flat;
                        break;
                    default:
                        pen.StartCap = LineCap.Flat;
                        pen.EndCap = LineCap.Flat;
                        break;
                }
                g.DrawLine(pen, this.DrawPointFrom, this.DrawPointTo);
            }

            // weight drawing.
            if (this.Weight != 0)
            {
                using (SolidBrush numberBrush = new SolidBrush(this.Selected ? _selectedFillColor : _weightColor))
                {
                    Font font = new Font("Arial", this._fontSize, FontStyle.Bold, GraphicsUnit.Pixel);
                    string text = this.Weight.ToString();

                    // centering the weight.
                    int middleX = 0;
                    if (this.DrawPointFrom.X > this.DrawPointTo.X)
                    {
                        middleX = this.DrawPointTo.X + (this.DrawPointFrom.X - this.DrawPointTo.X) / 2;
                    }
                    else
                    {
                        middleX = this.DrawPointFrom.X + (this.DrawPointTo.X - this.DrawPointFrom.X) / 2;
                    }
                    int middleY = 0;
                    if (this.DrawPointFrom.Y > this.DrawPointTo.Y)
                    {
                        middleY = this.DrawPointTo.Y + (this.DrawPointFrom.Y - this.DrawPointTo.Y) / 2;
                    }
                    else
                    {
                        middleY = this.DrawPointFrom.Y + (this.DrawPointTo.Y - this.DrawPointFrom.Y) / 2;
                    }
                    Point center = new Point(middleX, middleY);

                    g.DrawString(text, font, numberBrush, center);
                }
            }

        }

        public bool IsHit(Point p)
        {
            using (GraphicsPath graphicsPath = new GraphicsPath())
            {
                using (var pen = new Pen(Brushes.Black, this._width))
                {
                    graphicsPath.AddLine(this.DrawPointFrom, this.DrawPointTo);
                    return graphicsPath.IsOutlineVisible(p, pen);
                }
            }
        }

        public void ChangeArrowState()
        {
            this.ArrowDirection = (Direction)(((int)this.ArrowDirection + 1) % 3);
        }

        int IComparable<IGraphicsElement>.CompareTo(IGraphicsElement? other)
        {
            if (other.LatestActionTS >= this.LatestActionTS)
            {
                return -1;
            }
            return 1;
        }
    }
}
