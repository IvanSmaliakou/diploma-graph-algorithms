﻿namespace GraphAlgorithms.Drawing
{
    // ChangeNodeNumber will change node's number if it's greater than number in the deleted node.
    public delegate void DecreaseNodeNumberHandler(int deletedNodeNumber);

    // ChangeEdgePosition is a method template for changing edge's position.
    public delegate void ChangeEdgePosition();

    public class Node : IGraphicsElement
    {
        // NodeCenterChanged is an event of changing node's center.
        public event ChangeEdgePosition NodeCenterChanged;

        // NodeNumberCounter is a counter of nodes. It gives each node it's unique id-personal number.
        public static int NodeNumberCounter = 0;

        // selectedFillColor represents color of selected node.
        private Color _selectedFillColor = Color.Red;

        // normalFillColor represents color of normal state node.
        private Color _normalFillColor = Color.Gray;

        // CurrentNodeColor is a current node's color.
        public Color CurrentNodeColor = Color.Gray;

        // _algorithmSelectedColor is a color of node, that was selected by the algorithm.
        private Color _algorithmSelectedColor = Color.Yellow;

        // XToWidth is a quotient of X of node and drawingArea width.
        public double XToWidth { get; set; }

        // YToHeight is a quotient of Y of node and drawingArea height.
        public double YToHeight { get; set; }

        private readonly SolidBrush _numberBrush = new SolidBrush(Color.Green);

        // _font is a default text font for node number. 
        private readonly Font _textFont = new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.Pixel);

        // _borderPen is a border pen.
        private readonly Pen _borderPen = new Pen(Color.Black, 2);

        private bool _mouseDowned;
        public int Diameter { get; set; } = 50;

        public int NodeNumber { get; set; }

        public long LatestActionTS { get; set; }

        private Rectangle borders;

        private Point _center;
        public Point Center
        {
            get => this._center;

            set
            {
                this._center = value;
                this.NodeCenterChanged?.Invoke();
            }
        }

        public Point TextUpperLeft { get; set; }

        // MouseDowned indicates that mouse has been clicked, but not released.
        public bool LeftButtonMouseDowned
        {
            get { return _mouseDowned; }
            set
            {
                this._mouseDowned = value;
            }
        }

        private bool _selected = false;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (value)
                {
                    this.LatestActionTS = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                }
                this._selected = value;
            }
        }

        private bool _selectedByAlgorithm = false;
        public bool SelectedByAlgorithm { get => _selectedByAlgorithm; set => _selectedByAlgorithm = value; }

        private bool _currentlyAlgoSelected = false;
        public bool CurrentlyAlgoSelected { get => _currentlyAlgoSelected; set => _currentlyAlgoSelected = value; }

        // DenyLeftDownedButton will deny left downed button.
        public void DenyLeftDownedButton()
        {
            this.LeftButtonMouseDowned = false;
        }

        public static Tuple<Node, Node> GetNewNodesCoordinates(Node a, Node b, int diameter)
        {
            int dX = Math.Abs(a.Center.X - b.Center.X);
            int dY = Math.Abs(a.Center.Y - b.Center.Y);
            int distance = (int)Math.Sqrt(dX * dX + dY * dY);
            double decreaseCoeff = (double)(distance - diameter / 2) / (double)distance;
            double newAX = 0;
            double newBX = 0;
            double newAY = 0;
            double newBY = 0;
            if (a.Center.X >= b.Center.X)
            {
                newAX = b.Center.X + decreaseCoeff * dX;
                newBX = a.Center.X - decreaseCoeff * dX;
            }
            else
            {
                newBX = a.Center.X + decreaseCoeff * dX;
                newAX = b.Center.X - decreaseCoeff * dX;
            }
            if (a.Center.Y >= b.Center.Y)
            {
                newAY = b.Center.Y + decreaseCoeff * dY;
                newBY = a.Center.Y - decreaseCoeff * dY;
            }
            else
            {
                newBY = a.Center.Y + decreaseCoeff * dY;
                newAY = b.Center.Y - decreaseCoeff * dY;
            }
            Node ret1 = new Node((int)newAX, (int)newAY);
            Node ret2 = new Node((int)newBX, (int)newBY);
            return new Tuple<Node, Node>(ret1, ret2);
        }

        public void ChangeNodesState(IGraphicsElement clickedNode)
        {
            if (this.Equals(clickedNode))
            {
                this.Selected = !this.Selected;
                return;
            }
            this.SelectedByAlgorithm = false;
            this.Selected = false;
        }

        // DecreaseNodeNumber decreases node's number if it's more than the number in deleted node.
        public void DecreaseNodeNumber(int deletedNodeNumber)
        {
            if (deletedNodeNumber < this.NodeNumber)
            {
                this.NodeNumber--;
            }
        }

        // NewCenter defiles new node coordinates.
        public void NewCenter(int x, int y, double xToWidth, double yToHeight)
        {
            int left_x = x - Diameter / 2;
            int upper_y = y - Diameter / 2;
            this.borders = new Rectangle(new Point(left_x, upper_y), new Size(Diameter, Diameter));
            this.Center = new Point(x, y);
            this.LatestActionTS = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            double text_left_x = this.Center.X - Diameter / 3.5;
            double text_upper_y = this.Center.Y - Diameter / 2.82;
            this.TextUpperLeft = new Point((int)text_left_x, (int)text_upper_y);
            this.XToWidth = xToWidth;
            this.YToHeight = yToHeight;
        }

        // Fully functional model of a Node.
        public Node(ref ChangeSelectedElementState changeNodesStateEvent, ref DenyDownedButton denyButton, int x, int y, double xToWidth, double yToHeight)
        {
            // event handlers initialization
            denyButton += this.DenyLeftDownedButton;
            changeNodesStateEvent += this.ChangeNodesState;

            this.NodeNumber = ++NodeNumberCounter;
            this.LatestActionTS = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
            this.Center = new Point(x, y);
            int left_x = x - Diameter / 2;
            int upper_y = y - Diameter / 2;
            this.borders = new Rectangle(new Point(left_x, upper_y), new Size(Diameter, Diameter));

            double text_left_x = this.Center.X - Diameter / 3.5;
            double text_upper_y = this.Center.Y - Diameter / 2.82;
            this.TextUpperLeft = new Point((int)text_left_x, (int)text_upper_y);
            this.XToWidth = xToWidth;
            this.YToHeight = yToHeight;
        }

        // Node constructor requires center coordinates of a Node. MUST BE USED ONLY AS A NODE'S STRUCT CONSTRUCTOR! 
        public Node(int x, int y)
        {
            this.LatestActionTS = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
            this.Center = new Point(x, y);
            int left_x = x - Diameter / 2;
            int upper_y = y - Diameter / 2;
            this.borders = new Rectangle(new Point(left_x, upper_y), new Size(Diameter, Diameter));
        }

        // HitTest indicates wheither a point p is within node borders.
        public bool IsHit(Point p)
        {
            int dx = p.X - this.Center.X;
            int dy = p.Y - this.Center.Y;
            return dx * dx + dy * dy <= this.Diameter / 2 * this.Diameter / 2;
        }

        // Draw will draw the circle
        public void Draw(Graphics g)
        {
            Color color = _normalFillColor;
            if (this.CurrentlyAlgoSelected)
            {
                if (this.CurrentNodeColor == _algorithmSelectedColor)
                {
                    color = _normalFillColor;
                }
                if (this.CurrentNodeColor == _normalFillColor)
                {
                    color = _algorithmSelectedColor;
                }
            }
            else if (this.SelectedByAlgorithm)
            {
                color = _algorithmSelectedColor;
            }
            else if (this.Selected)
            {
                color = this._selectedFillColor;
            }
            var brush = new SolidBrush(color);
            g.FillEllipse(brush, this.borders);
            g.DrawEllipse(this._borderPen, this.borders);
            string number = this.NodeNumber.ToString();
            g.DrawString(number, this._textFont, this._numberBrush, this.TextUpperLeft);

            this.CurrentNodeColor = color;
        }

        int IComparable<IGraphicsElement>.CompareTo(IGraphicsElement? other)
        {
            if (other.LatestActionTS >= this.LatestActionTS)
            {
                return -1;
            }
            return 1;
        }
    }
}