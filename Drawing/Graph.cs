﻿namespace GraphAlgorithms.Drawing
{
    // ReturnEdgesToBeDeleted is a method template, that returns all edges, that was connected with deleting node.
    public delegate void ReturnEdgesToBeDeleted(Node connectedNode, NodeDeletedEventArgs eventArgs);

    // GetNeighborNode is a method template of appending neigbor node to eventArgs if node with numeber nodeNumber is contained within Edge.
    public delegate void GetNeighborNode(int nodeNumber, NodeNeighborsEventArgs eventArgs);

    // Graph is a container for all objects on the drawing screen.
    public class Graph
    {
        public List<IGraphicsElement> Nodes = new();
        public List<IGraphicsElement> Edges = new();

        // _graphics is needed for screen clearance.
        private Graphics _graphics;

        // DecreaseNodeNumber will decrease nodes number if it's greater than number of deleted node.
        public event DecreaseNodeNumberHandler DecreaseNodeNumber;

        public event ReturnEdgesToBeDeleted ReturnEdgesToBeDeleted;

        public event GetNeighborNode ReplyWithNeighborNodes;

        public CreateGraphicsDelegate CreateGraphics;
        public Graph(CreateGraphicsDelegate createGraphics)
        {
            this.CreateGraphics = createGraphics;
        }

        // AddNode adds a node to a Nodes list.
        public void AddNode(Node node)
        {
            this.DecreaseNodeNumber += node.DecreaseNodeNumber;
            this.Nodes.Add(node);
        }

        // AddEdge adds an Edge to edges list.
        public void AddEdge(Edge edge)
        {
            this.ReturnEdgesToBeDeleted += edge.ReturnEdgesConnectedWithNode;
            this.ReplyWithNeighborNodes += edge.GetNeighborNodes;
            this.Edges.Add(edge);
        }

        // RemoveNode removes a node.
        public void RemoveNode(Node node, ref ChangeSelectedElementState changeStateEvent, ref DenyDownedButton denyButton)
        {
            NodeDeletedEventArgs eventArgs = new NodeDeletedEventArgs();
            ReturnEdgesToBeDeleted?.Invoke(node, eventArgs);
            int nodeNumber = node.NodeNumber;

            bool ok = this.Nodes.Remove(node);
            if (ok)
            {
                // node cannot be garbage collected until the publishers have references on it.
                changeStateEvent -= node.ChangeNodesState;
                denyButton -= node.DenyLeftDownedButton;
                this.DecreaseNodeNumber -= node.DecreaseNodeNumber;

                this.DecreaseNodeNumber?.Invoke(nodeNumber);
                this.Edges = this.Edges.Except(eventArgs.Edges).ToList();
                Node.NodeNumberCounter--;
            }
        }

        public void RemoveEdge(Edge e, ref ChangeSelectedElementState changeStateEvent, ref DenyDownedButton denyButton, ref ChangeWeight increaseWeightEvent,
            ref ChangeWeight decreaseWeightEvent) {
            increaseWeightEvent -= e.IncreaseWeight;
            decreaseWeightEvent -= e.DecreaseWeight;
            denyButton -= e.DenyAllDownedButtons;
            changeStateEvent -= e.ChangeEdgesState;
            this.ReturnEdgesToBeDeleted -= e.ReturnEdgesConnectedWithNode;
            this.ReplyWithNeighborNodes -= e.GetNeighborNodes;
            e.FromNode.NodeCenterChanged -= e.SetNewDrawPointsCoordinates;
            e.FromNode.NodeCenterChanged -= e.SetNewDrawPointsCoordinates;
            this.Edges.Remove(e);
        }

        // GetObjectByCoords returns an object(Edge or Node) by provided coordinates.
        public Object GetObjectByCoords(int x, int y)
        {
            Point point = new(x, y);
            Object? ret = null;
            long maxLatestActionTS = long.MinValue;
            foreach (Node node in this.Nodes)
            {
                if (node.IsHit(point) && node.LatestActionTS >= maxLatestActionTS)
                {
                    maxLatestActionTS = node.LatestActionTS;
                    ret = node;
                }
            }
            foreach (Edge edge in this.Edges)
            {
                if (edge.IsHit(point) && edge.LatestActionTS >= maxLatestActionTS)
                {
                    maxLatestActionTS = edge.LatestActionTS;
                    ret = edge;
                }
            }
            return ret;
        }

        // CombineAllItems will return all elements: items and nodes.
        public List<IGraphicsElement> CombineAllItems()
        {
            return this.Nodes.Concat(this.Edges).ToList();
        }

        // DenySelectedByAlgorithmNodes will make normal all nodes, that was selected by algorithm.
        public void DenySelectedByAlgorithmNodes() {
            foreach (Node n in this.Nodes) {
                n.SelectedByAlgorithm = false;
                n.CurrentlyAlgoSelected = false;
            }
        }

        // RemoveSelectedGraphicsElement will remove selected graphics element from graph.
        public void RemoveSelectedGraphicsElement(ref ChangeSelectedElementState changeStateEvent, ref DenyDownedButton denyButton, ref ChangeWeight increaseWeightEvent,
            ref ChangeWeight decreaseWeightEvent)
        {
            List<IGraphicsElement> allItems = this.CombineAllItems();

            foreach (IGraphicsElement graphicsElement in allItems)
            {
                if (graphicsElement.Selected)
                {
                    if (graphicsElement is Node node)
                    {
                        this.RemoveNode(node, ref changeStateEvent, ref denyButton);
                    }
                    else
                    {
                        Edge e = graphicsElement as Edge;
                        this.RemoveEdge(e, ref changeStateEvent, ref denyButton, ref increaseWeightEvent, ref decreaseWeightEvent);
                    }

                }
            }
        }

        public List<Node> GetNeighborNodes(int nodeNumber)
        {
            NodeNeighborsEventArgs eventArgs = new NodeNeighborsEventArgs();
            this.ReplyWithNeighborNodes?.Invoke(nodeNumber, eventArgs);
            return eventArgs.nodes;
        }

        public Node? GetNodeByNumber(int number)
        {
            foreach (Node n in this.Nodes)
            {
                if (n.NodeNumber == number)
                {
                    return n;
                }
            }
            return null;
        }

        // GetLeftMouseDownedObject returns first selected graphics element. 
        public Object? GetLeftMouseDownedObject()
        {
            List<IGraphicsElement> allItems = this.CombineAllItems();
            foreach (IGraphicsElement el in allItems)
            {
                if (el.LeftButtonMouseDowned)
                {
                    return el;
                }
            }
            return null;
        }

        // GetSelectedNode returns selected Node. 
        public Node? GetSelectedNode()
        {
            foreach (Node n in this.Nodes)
            {
                if (n.Selected)
                {
                    return n;
                }
            }
            return null;
        }

        public void Redraw()
        {
            lock (this) {
                Graphics g = this.CreateGraphics();
                g.Clear(Color.FromKnownColor(KnownColor.Control));
                List<IGraphicsElement> allItems = this.CombineAllItems();
                allItems.Sort();
                foreach (IGraphicsElement el in allItems)
                {
                    el.Draw(g);
                }
            }
        }
    }
}
