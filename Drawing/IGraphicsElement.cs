﻿namespace GraphAlgorithms.Drawing
{
    // ChangeSelectedElementState delegate is a method template for methods, that changes element state from selected to non-selected
    // and vice versa.
    public delegate void ChangeSelectedElementState(IGraphicsElement clickedElement);

    // DenyDownedButton delegate is a method for denying downed button.
    public delegate void DenyDownedButton();

    // GraphicsElement defines behaviour for the graphics elements on the polygon.
    public interface IGraphicsElement : IComparable<IGraphicsElement>
    {
        long LatestActionTS { get; set; }

        bool Selected { get; set; }
        bool LeftButtonMouseDowned { get; set; }
        void Draw(Graphics g);
        bool IsHit(Point p);
    }
}
