﻿namespace GraphAlgorithms.Drawing
{
    public class NodeDeletedEventArgs : EventArgs
    {
        public List<Edge> Edges = new List<Edge>();
    }
}
