﻿namespace GraphAlgorithms.Drawing
{
    public class NodeNeighborsEventArgs: EventArgs
    {
        public List<Node> nodes = new List<Node>();
    }
}
