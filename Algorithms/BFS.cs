﻿using GraphAlgorithms.Drawing;

namespace GraphAlgorithms.Algorithms
{
    public class BFS
    {
        private Graph Graph { get; set; }

        public BFS(Graph g)
        {
            this.Graph = g;
        }

        public List<Node> MakeUndirectedBFS(int startingNodeNumber)
        {
            List<Node> result = new List<Node>();
            Queue<int> queue = new Queue<int>();
            queue.Enqueue(startingNodeNumber);
            Node startingNode = this.Graph.GetNodeByNumber(startingNodeNumber);
            result.Add(startingNode);
            while (queue.Count > 0) {
                int currentNodeNumber = queue.Dequeue();
                List<Node> neighbors = this.Graph.GetNeighborNodes(currentNodeNumber);
                foreach (Node neighbor in neighbors)
                {
                    if (!result.Contains(neighbor)) {
                        queue.Enqueue(neighbor.NodeNumber);
                        result.Add(neighbor);
                    }
                }
            }
        
            return result;
        }
    }
}
