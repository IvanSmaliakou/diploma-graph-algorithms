﻿using GraphAlgorithms.Drawing;

namespace GraphAlgorithms.Algorithms
{
    public class DFS
    {
        private Graph Graph { get; set; }

        public DFS(Graph g)
        {
            this.Graph = g;
        }

        public List<Node> MakeUndirectedDFS(int startingNodeNumber)
        {
            List<Node> result = new List<Node>();
            DFSWithNode(ref result, startingNodeNumber);
            return result;
        }

        private void DFSWithNode(ref List<Node> res, int nodeNumber)
        {
            Node? node = this.Graph.GetNodeByNumber(nodeNumber);
            res.Add(node);
            List<Node> neighbors = this.Graph.GetNeighborNodes(nodeNumber);
            foreach (Node neighbor in neighbors)
            {
                if (!res.Contains(neighbor))
                {
                    DFSWithNode(ref res, neighbor.NodeNumber);
                    res.Add(node);
                }
            }
        }
    }
}
