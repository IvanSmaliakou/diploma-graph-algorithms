﻿namespace GraphAlgorithms
{
    partial class BootstrapForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BootstrapForm));
            this.buttonCollapseExpandMenu = new System.Windows.Forms.Button();
            this.buttonDFS = new System.Windows.Forms.Button();
            this.bfsButton = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.problemAreaPanel = new System.Windows.Forms.Panel();
            this.problemAreaTextBox = new System.Windows.Forms.RichTextBox();
            this.codeAreaPanel = new System.Windows.Forms.Panel();
            this.codeAreaTextBox = new System.Windows.Forms.RichTextBox();
            this.drawingAreaPanel = new System.Windows.Forms.Panel();
            this.algoPathTextBox = new System.Windows.Forms.RichTextBox();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonNextNode = new System.Windows.Forms.Button();
            this.buttonPreviousNode = new System.Windows.Forms.Button();
            this.buttonsAreaPanel = new System.Windows.Forms.Panel();
            this.buttonAbout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.problemAreaPanel.SuspendLayout();
            this.codeAreaPanel.SuspendLayout();
            this.drawingAreaPanel.SuspendLayout();
            this.buttonsAreaPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCollapseExpandMenu
            // 
            this.buttonCollapseExpandMenu.BackColor = System.Drawing.SystemColors.Control;
            this.buttonCollapseExpandMenu.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonCollapseExpandMenu.Image = global::GraphAlgorithms.Properties.Resources.icons8_двойная_стрелка_влево_26;
            this.buttonCollapseExpandMenu.Location = new System.Drawing.Point(252, 0);
            this.buttonCollapseExpandMenu.Name = "buttonCollapseExpandMenu";
            this.buttonCollapseExpandMenu.Size = new System.Drawing.Size(41, 1057);
            this.buttonCollapseExpandMenu.TabIndex = 1;
            this.buttonCollapseExpandMenu.UseVisualStyleBackColor = false;
            this.buttonCollapseExpandMenu.Click += new System.EventHandler(this.OnButtonCollapseExpandMenuClick);
            // 
            // buttonDFS
            // 
            this.buttonDFS.Location = new System.Drawing.Point(3, 0);
            this.buttonDFS.Name = "buttonDFS";
            this.buttonDFS.Size = new System.Drawing.Size(254, 80);
            this.buttonDFS.TabIndex = 3;
            this.buttonDFS.Text = "Поиск в глубину";
            this.buttonDFS.UseVisualStyleBackColor = true;
            this.buttonDFS.Click += new System.EventHandler(this.buttonDFSClick);
            // 
            // bfsButton
            // 
            this.bfsButton.Location = new System.Drawing.Point(0, 86);
            this.bfsButton.Name = "bfsButton";
            this.bfsButton.Size = new System.Drawing.Size(257, 80);
            this.bfsButton.TabIndex = 2;
            this.bfsButton.Text = "Поиск в ширину";
            this.bfsButton.UseVisualStyleBackColor = true;
            this.bfsButton.Click += new System.EventHandler(this.bfsButtonClick);
            // 
            // problemAreaPanel
            // 
            this.problemAreaPanel.BackColor = System.Drawing.SystemColors.Control;
            this.problemAreaPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.problemAreaPanel.Controls.Add(this.problemAreaTextBox);
            this.problemAreaPanel.Location = new System.Drawing.Point(293, 1);
            this.problemAreaPanel.Name = "problemAreaPanel";
            this.problemAreaPanel.Size = new System.Drawing.Size(0, 1057);
            this.problemAreaPanel.TabIndex = 0;
            // 
            // problemAreaTextBox
            // 
            this.problemAreaTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.problemAreaTextBox.Location = new System.Drawing.Point(0, 0);
            this.problemAreaTextBox.Name = "problemAreaTextBox";
            this.problemAreaTextBox.ReadOnly = true;
            this.problemAreaTextBox.Size = new System.Drawing.Size(0, 1057);
            this.problemAreaTextBox.TabIndex = 1;
            this.problemAreaTextBox.Text = resources.GetString("problemAreaTextBox.Text");
            // 
            // codeAreaPanel
            // 
            this.codeAreaPanel.BackColor = System.Drawing.SystemColors.Control;
            this.codeAreaPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.codeAreaPanel.Controls.Add(this.codeAreaTextBox);
            this.codeAreaPanel.ForeColor = System.Drawing.SystemColors.Control;
            this.codeAreaPanel.Location = new System.Drawing.Point(677, 1);
            this.codeAreaPanel.Name = "codeAreaPanel";
            this.codeAreaPanel.Size = new System.Drawing.Size(0, 1057);
            this.codeAreaPanel.TabIndex = 1;
            // 
            // codeAreaTextBox
            // 
            this.codeAreaTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.codeAreaTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.codeAreaTextBox.Location = new System.Drawing.Point(0, 0);
            this.codeAreaTextBox.Name = "codeAreaTextBox";
            this.codeAreaTextBox.Size = new System.Drawing.Size(0, 1057);
            this.codeAreaTextBox.TabIndex = 0;
            this.codeAreaTextBox.Text = "";
            // 
            // drawingAreaPanel
            // 
            this.drawingAreaPanel.BackColor = System.Drawing.SystemColors.Control;
            this.drawingAreaPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.drawingAreaPanel.Controls.Add(this.algoPathTextBox);
            this.drawingAreaPanel.Controls.Add(this.buttonStop);
            this.drawingAreaPanel.Controls.Add(this.buttonStart);
            this.drawingAreaPanel.Controls.Add(this.buttonNextNode);
            this.drawingAreaPanel.Controls.Add(this.buttonPreviousNode);
            this.drawingAreaPanel.Location = new System.Drawing.Point(293, 1);
            this.drawingAreaPanel.Name = "drawingAreaPanel";
            this.drawingAreaPanel.Size = new System.Drawing.Size(1228, 1057);
            this.drawingAreaPanel.TabIndex = 2;
            this.drawingAreaPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnDrawingPanelMouseDown);
            this.drawingAreaPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnDrawingPanelMouseMove);
            this.drawingAreaPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnDrawingPanelMouseUp);
            // 
            // algoPathTextBox
            // 
            this.algoPathTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.algoPathTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.algoPathTextBox.Enabled = false;
            this.algoPathTextBox.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.algoPathTextBox.Location = new System.Drawing.Point(-2, 923);
            this.algoPathTextBox.Name = "algoPathTextBox";
            this.algoPathTextBox.Size = new System.Drawing.Size(418, 0);
            this.algoPathTextBox.TabIndex = 5;
            this.algoPathTextBox.Text = "";
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Image = global::GraphAlgorithms.Properties.Resources.Stop_sign__1_;
            this.buttonStop.Location = new System.Drawing.Point(44, -1);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(48, 48);
            this.buttonStop.TabIndex = 3;
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStopClick);
            // 
            // buttonStart
            // 
            this.buttonStart.Image = global::GraphAlgorithms.Properties.Resources.execute_png_7__1___1_;
            this.buttonStart.Location = new System.Drawing.Point(-2, -2);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(48, 48);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStartClick);
            // 
            // buttonNextNode
            // 
            this.buttonNextNode.Enabled = false;
            this.buttonNextNode.Image = global::GraphAlgorithms.Properties.Resources.image;
            this.buttonNextNode.Location = new System.Drawing.Point(216, 0);
            this.buttonNextNode.Name = "buttonNextNode";
            this.buttonNextNode.Size = new System.Drawing.Size(48, 48);
            this.buttonNextNode.TabIndex = 1;
            this.buttonNextNode.UseVisualStyleBackColor = true;
            this.buttonNextNode.Click += new System.EventHandler(this.buttonNextNodeClick);
            // 
            // buttonPreviousNode
            // 
            this.buttonPreviousNode.Enabled = false;
            this.buttonPreviousNode.Image = global::GraphAlgorithms.Properties.Resources.images__1___1_;
            this.buttonPreviousNode.Location = new System.Drawing.Point(162, 0);
            this.buttonPreviousNode.Name = "buttonPreviousNode";
            this.buttonPreviousNode.Size = new System.Drawing.Size(48, 48);
            this.buttonPreviousNode.TabIndex = 0;
            this.buttonPreviousNode.UseVisualStyleBackColor = true;
            this.buttonPreviousNode.Click += new System.EventHandler(this.buttonPreviousNodeClick);
            // 
            // buttonsAreaPanel
            // 
            this.buttonsAreaPanel.Controls.Add(this.buttonCollapseExpandMenu);
            this.buttonsAreaPanel.Controls.Add(this.buttonAbout);
            this.buttonsAreaPanel.Controls.Add(this.buttonDFS);
            this.buttonsAreaPanel.Controls.Add(this.bfsButton);
            this.buttonsAreaPanel.Location = new System.Drawing.Point(0, 1);
            this.buttonsAreaPanel.Name = "buttonsAreaPanel";
            this.buttonsAreaPanel.Size = new System.Drawing.Size(292, 1057);
            this.buttonsAreaPanel.TabIndex = 4;
            // 
            // buttonAbout
            // 
            this.buttonAbout.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonAbout.Image = global::GraphAlgorithms.Properties.Resources.about__1_;
            this.buttonAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAbout.Location = new System.Drawing.Point(3, 980);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(254, 74);
            this.buttonAbout.TabIndex = 4;
            this.buttonAbout.Text = "Справка";
            this.buttonAbout.UseVisualStyleBackColor = true;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // BootstrapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1533, 1057);
            this.Controls.Add(this.drawingAreaPanel);
            this.Controls.Add(this.buttonsAreaPanel);
            this.Controls.Add(this.codeAreaPanel);
            this.Controls.Add(this.problemAreaPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "BootstrapForm";
            this.Text = "ПОМ \"Основные алгоритмы обхода графа\"";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Resize += new System.EventHandler(this.OnResize);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.problemAreaPanel.ResumeLayout(false);
            this.codeAreaPanel.ResumeLayout(false);
            this.drawingAreaPanel.ResumeLayout(false);
            this.buttonsAreaPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Button buttonCollapseExpandMenu;
        private BindingSource bindingSource1;
        private Button buttonDFS;
        private Button bfsButton;
        private ToolTip toolTip1;
        private Panel problemAreaPanel;
        private Panel codeAreaPanel;
        private Panel drawingAreaPanel;
        private Panel buttonsAreaPanel;
        private RichTextBox problemAreaTextBox;
        private Button buttonNextNode;
        private Button buttonPreviousNode;
        private Button buttonStop;
        private Button buttonStart;
        private RichTextBox codeAreaTextBox;
        private RichTextBox algoPathTextBox;
        private Button buttonAbout;
    }
}